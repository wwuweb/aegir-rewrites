<?php

/**
 * @file
 * Custom apache rewrites for Drupal sites.
 */

/**
 * Implements hook_provision_apache_vhost_confi().
 */
function rewrites_provision_apache_vhost_config($uri, $data) {
  if ($uri === 'www.wwu.edu' || preg_match('@^www(prod|dev|test)\d+x\d+\.wwu\.edu$@', $uri) === 1) {
    $config[] = '';

    $config[] = '<IfModule mod_rewrite.c>';
    $config[] = '  RewriteEngine on';
    $config[] = '';
    $config[] = '  Include /var/aegir/.drush/wwu-rewrites/*.conf';
    $config[] = '</IfModule>';

    $config[] = '';

    return $config;
  }

  if ($uri === 'studyabroad.wwu.edu' || preg_match('@^studyabroad(prod|dev|test)\d+x\d+\.wwu\.edu$@', $uri) === 1) {
    $config[] = '';

    $config[] = '<IfModule mod_rewrite.c>';
    $config[] = '  RewriteEngine on';
    $config[] = '';
    $config[] = '  RewriteCond %{QUERY_STRING} ^go=(.*)$';
    $config[] = '  RewriteRule ^/$ https://wwu-sa.terradotta.com/?go=%1 [redirect=permanent,last]';
    $config[] = '';
    $config[] = '  RewriteCond %{QUERY_STRING} ^FuseAction=(.*)$';
    $config[] = '  RewriteRule ^/index.cfm https://wwu-sa.terradotta.com/index.cfm?FuseAction=%1 [redirect=permanent,last]';
    $config[] = '</IfModule>';

    $config[] = '';

    return $config;
  }
}
